<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Tarefas;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class TarefasController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    public function index()
    {
        try {
            $tarefas = Tarefas::all();
            return response()->json([
                'status' => 'success',
                'tarefas' => $tarefas,
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'status' => 'error',
                'message' => 'Erro ao buscar tarefas',
            ], 500);
        }
    }

    public function store(Request $request)
    {
        try {
            $request->validate([
                'title' => 'required|string|max:255',
                'description' => 'required|string|max:255',
            ]);

            $tarefa = Tarefas::create([
                'title' => $request->title,
                'description' => $request->description,
            ]);

            return response()->json([
                'status' => 'success',
                'message' => 'Tarefa criada com sucesso',
                'tarefa' => $tarefa,
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'status' => 'error',
                'message' => 'Erro ao criar tarefa',
            ], 500);
        }
    }

    public function show($id)
    {
        try {
            $tarefa = Tarefas::findOrFail($id);
            return response()->json([
                'status' => 'success',
                'tarefa' => $tarefa,
            ]);
        } catch (ModelNotFoundException $e) {
            return response()->json([
                'status' => 'error',
                'message' => 'Tarefa não encontrada',
            ], 404);
        } catch (\Exception $e) {
            return response()->json([
                'status' => 'error',
                'message' => 'Erro ao buscar tarefa',
            ], 500);
        }
    }

    public function update(Request $request, $id)
    {
        try {
            $request->validate([
                'title' => 'required|string|max:255',
                'description' => 'required|string|max:255',
            ]);

            $tarefa = Tarefas::findOrFail($id);
            $tarefa->title = $request->title;
            $tarefa->description = $request->description;
            $tarefa->save();

            return response()->json([
                'status' => 'success',
                'message' => 'Tarefa atualizada com sucesso',
                'tarefa' => $tarefa,
            ]);
        } catch (ModelNotFoundException $e) {
            return response()->json([
                'status' => 'error',
                'message' => 'Tarefa não encontrada',
            ], 404);
        } catch (\Exception $e) {
            return response()->json([
                'status' => 'error',
                'message' => 'Erro ao atualizar tarefa',
            ], 500);
        }
    }

    public function destroy($id)
    {
        try {
            $tarefa = Tarefas::findOrFail($id);

            if (!$tarefa->delete()) {
                throw new \Exception('Erro ao deletar tarefa');
            }

            return response()->json([
                'status' => 'success',
                'message' => 'Tarefa deletada com sucesso',
                'tarefa' => $tarefa,
            ]);
        } catch (ModelNotFoundException $e) {
            return response()->json([
                'status' => 'error',
                'message' => 'Tarefa não encontrada',
            ], 404);
        } catch (\Exception $e) {
            return response()->json([
                'status' => 'error',
                'message' => $e->getMessage(),
            ], 500);
        }
    }
}
