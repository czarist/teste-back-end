# Simple CRUD list application using laravel api with JWT authentication 

### Requirements

    1. PHP version upto 8.0.*    
    2. Laravel version  9.5.*   
    3. MySQL version 5.7.* > 

### Installation

    1.  Clone this project using git clone. 
        Clone este projeto usando git clone.

    2.  Copy a fresh .env file from laravel github.
        Copie um novo arquivo .env do laravel github.

    3.  Update .env file by adding database information like tarefas.  
        Atualize o arquivo .env adicionando informações do banco de dados, como tarefas.

    4.  Go to project root folder.Open git bash or terminal and run composer install. 
        Vá para a pasta raiz do projeto. Abra o git bash ou terminal e execute composer install.

    5.  Run php artisan key:generate in the terminal.
        Execute php artisan key:generate no terminal.

    6.  Run php artisan jwt:secret in the terminal.   
        Execute php artisan jwt:secret no terminal.

    7.  Create a database in localhost named tarefas.     
        Crie um banco de dados no localhost com o nome de tarefas.

    8.  Run php artisan migrate. 
        Execute php artisan migrate.

    9.  Run php artisan serve.
        Execute php artisan serve.

    10. Follow the instructions in the images to test the endpoints using Postman or similar.
        Siga as instruções nas imagens para testar os endpoints usando Postman ou similar.

### Register
![Register](readmeImg/register.png)

### Login
![Login](readmeImg/login.png)

### Token

    The refresh, logout, and tarefas endpoints are all protected by the auth:api middleware and therefore require that we send a valid token with the authorization header.
    Os endpoints de refresh, logout e tarefas são todos protegidos pelo middleware auth:api e, portanto, exigem que enviemos um token válido com o cabeçalho de autorização.

![Token](readmeImg/token.png)


### Refresh
    After testing the refresh endpoint, it is necessary to update the authorization token.
    Depois de testar o endpoint de refresh é preciso atualizar o token de autorização.

![Refresh](readmeImg/refresh.png)

### Logout
    After testing the logout endpoint, it is necessary to update the authorization token.
    Depois de testar o endpoint de logout é preciso atualizar o token de autorização. 

![Logout](readmeImg/logout.png)

### Create Tarefa
![Create](readmeImg/createtarefa.png)

### List Tarefas
![List](readmeImg/list.png)

### Show Tarefa by ID
![Show](readmeImg/show.png)

### Update Tarefa
![Update](readmeImg/update.png)

### Delete Tarefa
![delete](readmeImg/delete.png)

